# Lists 
# Lists are similar to arrays in JavaScript in a sense that they can contain a collection of data
# To create a list, the square bracket ([]) is used.
names = ["John", "Paul", "George", "Ringo"]
programs = ['developer career', 'pi-shape', 'short courses']
durations = [260, 180, 20]
truth_values = [True, False, True, True, False]

# Example of a list with different data types
sample_list = ["Apple", 3, False, "Potato", 4, True] 
print(sample_list)

# Getting the size or the number of elements of the list
print(len(programs))

# Access values of the list
# By using the index number of the element 
# Access the first item/element in the list
print(programs[0]) 

# Access the second item/element in the list
print(durations[1])

# Access the second item/element in the list
print(programs[-1])

# Access the whole list/all elements
print(durations)

# Access a range of values 
# list [start index: end index]
# programs[starts at 0: access 2 items]
# end index is not included in accessing
print(programs[0:2])

# Updating the list 
# Print the current value 
print(f'Current value: {programs[2]}')

# Update the value by accessing the programs list
programs[2] = 'Short Courses XD'

# After updating the element print the new value
print(f'New value: {programs[2]}')

# Mini-exercise
# 1. Create a list of names of 5 students
# 2. Create a list of grades for the 5 students
# 3. Use a loop to iterate through the lists printing in following format:
# The grade of John is 100
lists_name = ["Mark", "Ivan", "Angelo", "Christ", "Dave"]

list_grades = [70, 80, 90, 100, 75]

for x in range(len(lists_name)):
		print(f"The grade of {lists_name[x]} is {list_grades[x]}")

# List Manipulation
# List has methods that can be used to manipulate the elements within

# Adding List Items - the append() allows to insert items to a list
programs.append('global')
print(programs)

# Deleting List Items - the 'del' keyword can be used to delete elements in the list
# Add new item to the duration list
durations.append(360)
print(durations)

# Delete the last item on the list
del durations[-1]
print(durations)

# Membership checks - the 'in' keyword checks if the elements is in the list
# Returns true or false
print(20 in durations)
print(500 in durations)

# Sorting lists - the sort() method sorts the list alphanumerically, ascending by default
print(names)
names.sort()
print(names)

# Emptying the list - the clear() method

test_list = [1, 3, 5, 7, 9]
print(test_list)
test_list.clear()
print(test_list)

# Dictionaries are used to store data values in our key:value pairs are denoted with (key : value).

person1 = {
	"name": "Dhony",
	"age": 18,
	"occupation": "student",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

print(person1)

# To get the number of key-value pairs in the dictionary, the len() method can be used
print(len(person1))

# Accessing values in the dictionary
print(person1["name"])

# The keys() method will return a list of all the keys in the dictionary
print(person1.keys())

# The values() method will return a list of all the values in the dictionary
print(person1.values())

# The items() method will return each item in a dictionary as a key-value pair.
print(person1.items())

# Adding the key-value pairs can be done either putting a new index key and assigning a value or the update() method
person1["nationality"] = "Filipino"
person1.update({"fave_food": "Sinigang"})
print(person1)

# Deleting entries can be done using the pop() method or the del keyword.
person1.pop("fave_food")
del person1["nationality"]
print(person1)

# clear() method empties the dictionary
person2 = {
	"name": "John",
	"age": 25
}

print("PERSON 2: ", end = '\n')
print(person2)
person2.clear()
print(person2)

# Looping through dictionaries
for key in person1:
	print(f"The value of {key} is {person1[key]}")

# Nested Dictionaries - dictionaries can be nested inside each other
person3 = {
	"name": "Hilda",
	"age": 20,
	"occupation": "writer",
	"isEnrolled": True,
	"subjects": ["Python", "SQL", "Django"]
}

classRoom = {
	"student1" : person1,
	"student2" : person3,
}

print(classRoom)

# Mini-Exercise
# 1. Create a car dictionary with the following keys
# brand, model, year of make, color
# 2. Print the following statement from the details:
# "I own a <brand> <model> and it was made in <year of make>"

car = {
	"brand" : "Honda",
	"model" : "subcompact",
	"yearOfMake" : 2022,
	"color" : "#70023f"
}

print("I own a " + car["brand"] + " " + car["model"] + " and it was made in " + str(car["yearOfMake"]))

# Functions
# Functions are blocks of code that runs when called The "def" keyword is used to create a function. The syntax is def <functionName>()

def my_greeting():
	# code to be run when my_greeting is called back
	print('Hello User')

# Calling/Invoke a function
my_greeting()

# Parameters can be added to functions to have more control to what the inputs for the function will be
def greet_user(username):
	# prints out the value of the username parameter
 	print(f'Hello, {username}!')

greet_user('Dhony')
greet_user('Stacy')

# From a function's perspective:
# A parameter is a variable listed inside the parentheses in the function definition
# Argument is the value that is sent to the function when it is called.

# return statemnt - the 'return' keyword allow functions return values

def addition(x, y):
	return x + y

sum = addition(1, 23)
print(f"The sum is {sum}.")

print(f"The sum is {addition(1, 24)}")

# Lambda Functions
# A lambda function is a small anonymous function that can be used for callbacks.


greeting = lambda person : f'Hello {person}'
print(greeting('Mark'))
print(greeting('Stacy'))

multiply = lambda a, b : a * b
print(multiply(2, 35))
print(multiply(66, 25))


# Classes
# Classes would serve as blueprints to describe the concepts of objects
# To create a class, the 'class' keyword is used along with the class name that starts with an uppercase character
# class ClassName()
# class ClassName:

class Car():
	# properties
	def __init__(self, brand, model, year_of_make):
		self.brand = brand
		self.model = model
		self.year_of_make = year_of_make

		# other properties can be added and assigned hard-coded values
		self.fuel = "Gasoline"
		self.fuel_level = 0

	# methods
	def fill_fuel(self):
		print(f'Current fuel level: {self.fuel_level}')
		print(f'Filling up the fuel tank ...')
		self.fuel_level = 100
		print(f'New fuel level: {self.fuel_level}')

	def drive(self, distance):
		print(f'The car is driven {distance} kilometers.')
		print(f'The fuel level left: {self.fuel_level - distance}')

# Creating a new instance is done by calling the class and provide the arguments
new_car = Car('Honda', 'GT-R', '2019')

# Displaying attributes can be done using the dot notation

print(f'My car is a {new_car.brand} {new_car.model}')

# Calling methods of the instance
new_car.fill_fuel()
new_car.drive(23)